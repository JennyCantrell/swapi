<?php

namespace swapi\utilities;
include "utilities\CurlWrapper.php";
use swapi\utilities\CurlWrapper;

Class SwapiApiClient {
    const BASE_URL = "https://swapi.co/api/";
    public $objects = [
        "planets",
        "starships",
        "vehicles",
        "people",
        "films",
        "species",
    ];
    private $cache = [];

    public function getData($data_name, $data_id = 0)
    {
        if (!empty($this->cache[$data_name]) && !$data_id) {
            return $this->cache[$data_name];
        } else if (!empty($this->cache[$data_name][$data_id])) {
            return $this->cache[$data_name][$data_id];
        }

        $url = self::BASE_URL.$data_name;

        if ($data_id) {
            $url .= "/".$data_id;
            $result = CurlWrapper::getOne($url);
        } else {
            $result = CurlWrapper::getAll($url);
        }

        $this->cache[$data_name] = $result;
        return $result;
    }

    public function condenseUrls($bject)
    {
        // replace url values with the name/title of the actual object
    }
}
