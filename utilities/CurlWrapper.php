<?php

namespace swapi\utilities;

Class CurlWrapper {
    public static function getAll($url)
    {
        $results = [];
        while ($url) {
            $response = CurlWrapper::getOne($url);
            if (empty($response)) {
                return false;
            }
            $url = !empty($response["next"]) ? $response["next"] : false;
            $results = array_merge($results, $response["results"]);
        }
        return $results;
    }

    public static function getOne($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: application/json"]);

        $response = curl_exec($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        if ($curl_error) {
            echo "cURL Error: ".$curl_error."\n";
            return false;
        } else {
            return json_decode($response, true);
        }
    }

    public static function post($url, $data)
    {

    }
}
