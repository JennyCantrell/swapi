<?php

namespace swapi;
include "utilities\SwapiApiClient.php";
use swapi\utilities\SwapiApiClient;

$swapi = new SwapiApiClient();
$handle = fopen ("php://stdin","r");
$continue = true;

echo "Welcome to SWAPI!\n";
while (displayEntities($swapi, $handle)) {}
fclose($handle);

function displayEntities(&$swapi, &$handle)
{
    echo "\nWhat would you like to know about?\n";
    foreach ($swapi->objects as $key => $value) {
        echo "(".$key.") ".$value."\n";
    }
    echo "\n";

    $entity_index = trim(fgets($handle));
    if (!is_numeric($entity_index)) {
        return handleStringInput($entity_index);
    } else if (empty($swapi->objects[$entity_index])) {
        echo "I do not understand your input.\n";
        return true;
    } else {
        return displayResults($swapi, $handle, $entity_index);
    }
}

function displayResults(&$swapi, &$handle, $entity_index)
{
    $results = $swapi->getData($swapi->objects[$entity_index]);

    if ($results === false) {
        echo "There was a problem getting the requested data";
        return true;
    }

    foreach ($results as $key => $result) {
        $display = !empty($result['name']) ? $result['name'] : $result['title'];
        echo "(".$key.") ".$display."\n";
    }
    echo "\n";

    $specific_index = trim(fgets($handle));
    if (!is_numeric($specific_index)) {
        return handleStringInput($specific_index);
    } else if (empty($results[$specific_index])) {
        echo "I do not understand your input.\n";
        return true;
    } else {
        print_r($results[$specific_index]);
        echo "\n";
        return true;
    }
}

function handleStringInput($input)
{
    if ($input == 'exit') {
        echo "Thank you for using SWAPI! Goodbye.\n";
        $continue = false;
    } else if ($input == 'help') {
        echo "Enter 'exit' at anytime to exit.\n";
        echo "or 'reset' to return to the begining.\n";
        $continue = true;
    } else if ($input == 'reset') {
        $continue = true;
    } else {
        echo "I do not understand your input.\n";
        $continue = true;
    }

    return $continue;
}